from math import *
import re
import os
import time


def licz_funkcje(funkcja,x):
    funkcja_n = funkcja.replace('x',str(x))
    try:
        funkcja_n = eval(funkcja)
    except:
        funkcja_n = None

    return funkcja_n


separator = "-"
separator=separator.center(20,separator)


print "Poszukamy sobie pierwiastow funkcji :)\nAutor: Maksym Kawelski\n"
print separator

# podawanie funkcji przez uzytkownika i sprawdzanie jej poprawnosci pod wzgledem "merytorycznym"
blad=0


while True:
    if blad==1:
        #wystapil blad - ponowna proba
        os.system('CLS')
        blad=0
        print "wystapil blad - brak zmiennej w funkcji!\n\n"
    if blad==3:
        #zly zapis funkcji pod wzgledem merytorycznym
        os.system('CLS')
        blad=0
        print "zly zapis funkcji - sproboj jeszcze raz"

    funkcja = raw_input("podaj funkcje f(x)=")

    #szukanie i okreslanie zmiennych w podanej funkcji - uboga wersja, szuka tylko x i sprawdza czy funkcja jest poprawnie podana
    zmienne = re.findall("[x]",str(funkcja))
    ile_zmiennych = len(zmienne)

    if ile_zmiennych==0:
        #brak zmiennej podanej - cofniecie
        blad = 1
    else:
        #wszystko git - sprawdzanie poprawnosci napisanej funkcji
        zmienne=zmienne[0]
        funkcja_test = funkcja
        funkcja_test=funkcja_test.replace(zmienne,'1')

        #print funkcja_test

        try:
            wynik=eval(funkcja_test)
            blad=0
            del(funkcja_test)
            del(ile_zmiennych)
            konsola = "f(x)=%s" % (str(funkcja))
            #print wynik
            break
        except:
            blad=3

#okreslanie przedzialu liczbowego dla ktorego bedziemy szukac pierwiastkow

os.system('CLS')
print konsola
print separator

while True:
    try:
        od = float(input("Podaj poczatek przedzialu: "))
        break
    except:
        os.system('CLS')
        print konsola
        print separator
        print "podana wartosc musi byc liczba!"

while True:
    try:
        do = float(input("Podaj koniec przedzialu: "))
        break
    except:
        os.system('CLS')
        print konsola
        print separator
        print "podana wartosc musi byc liczba!"

konsola = konsola+"\nPrzedzial: (%s,%s)" % (od,do)

os.system('CLS')
print konsola
print separator

#okreslenie co ile ma nastepowac przeskok x'a
while True:
    try:
        przeskok=float(input("Podaj wartosc co ile ma nastepowac przeskok w x'ie: "))
        break
    except:
        os.system('CLS')
        print konsola
        print separator
        print "podana wartosc musi byc liczba!"

konsola = konsola+"\nPrzeskok x'a: %s" % (przeskok)

os.system('CLS')
print konsola
print separator

#utworzenie pliku z wynikami
file_name = time.localtime()
file_name = "%s-%s-%s %s-%s-%s.html" % (file_name[0],file_name[1],file_name[2],file_name[3],file_name[4],file_name[5])
uchwyt = open(file_name,"w+")
info = """
funkcja: f(x)=%s<br>
przedzial: (%s,%s)<br>
przeskok: %s<br><br>
""" % (funkcja,od,do,przeskok)
uchwyt.writelines(info)



pierwszy=0
zerowe = []
licznik=od
koniec = ""
pop_y=None

uchwyt.write("<table border='1'><tr><td>x</td><td>f(x)</td></tr>")

html_pisz = ""

while True:
    if pierwszy==0:
        print ""
    else:
        licznik=licznik+przeskok

    #print licznik
    funkcja_test=funkcja
    funkcja_test=funkcja_test.replace('x',str(licznik))
    #print funkcja_test
    #print type(funkcja_test)

    #raw_input()

    try:
        wynik_funk = eval(funkcja_test)
        #print round(wynik_funk,10)
    except:
        wynik_funk = "error"

    if round(licznik,10)==round(0,10):
        licznik=fabs(licznik)

    #print wynik_funk
    #print type(wynik_funk)
    #raw_input()


    pisz = "<tr><td>%s</td><td>%s</td></tr>" % (licznik,wynik_funk)
    #print pisz
    uchwyt.write(pisz)

    if wynik_funk!="error" and pop_y!=None:

        if pierwszy==1:
            #print "%s oraz %s" % (pop_y,round(wynik_funk,10))
            if ((pop_y<=0) and (round(float(wynik_funk),10)>0)) or ((pop_y>=0) and (round(float(wynik_funk),10)<0)):
                html_pisz += "<b>miejsce przegiecia dla x: %s i %s</b><br>" % (pop_x,licznik)
                koniec += "miejsce przegiecia dla x: %s i %s\n" % (pop_x,licznik)
                zerowe.append([round(pop_x,10),round(licznik,10)])

        #print wynik_funk

    pop_x=round(float(licznik),10)
    if wynik_funk!="error":
        pop_y=round(wynik_funk,10)
    else:
        pop_y=None

        #raw_input()

    if pierwszy==0:
        pierwszy=1

    if round(licznik,10)==round(do,10):
        break

print koniec

uchwyt.write("</table><br><br>"+html_pisz+"<br><br>")

koniec = ""

pierwszy=0

html_pisz = ""

if len(zerowe)==0:
    print "brak miejsc zerowych"
    uchwyt.write("brak miejsc zerowych")
else:

    #pytanie o dokladnosc!
    while True:
        try:
            dokladnosc = int(input("Podaj dokladnosc szukania (ile miejsc po przecinku): "))
            break
        except:
            os.system('CLS')
            print konsola
            print separator
            print "podana wartosc musi byc liczba!"


    for x in range(0,len(zerowe)):
        #print zerowe[x][0]
        pisz = "<b>Szukanie dla %s oraz %s</b><br><br><table border='1'><tr><td>x1</td><td>x2</td><td>x3</td><td>f(x1)</td><td>f(x2)</td><td>f(x3)</td></tr>" % (zerowe[x][0],zerowe[x][1])
        uchwyt.write(pisz)
        while True:
            if pierwszy==0:
                #pierwsza petla
                x1 = zerowe[x][0]
                x2 = zerowe[x][1]
                x3 = (zerowe[x][0]+zerowe[x][1])/2

                f1 = licz_funkcje(funkcja,x1)
                f2 = licz_funkcje(funkcja,x2)
                f3 = licz_funkcje(funkcja,x3)

                pisz = "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (x1,x2,x3,f1,f2,f3)
                #print pisz
                uchwyt.write(pisz)
                pierwszy=1
            else:
                if f1*f3>0:
                    x1=x3_p
                    x2=x2_p
                else:
                    x1=x1_p
                    x2=x3_p

                x3 = (x1+x2)/2
                f1 = licz_funkcje(funkcja,x1)
                f2 = licz_funkcje(funkcja,x2)
                f3 = licz_funkcje(funkcja,x3)

                pisz = "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (x1,x2,x3,f1,f2,f3)
                #print pisz
                uchwyt.write(pisz)

            if round(x1,dokladnosc)==round(x2,dokladnosc)==round(x3,dokladnosc):
                html_pisz += "<b>Pierwiastek w: %s</b><br>" % (x1)
                koniec += "Pierwiastek w: %s\n" % (x1)
                break

            x1_p=x1
            x2_p=x2
            x3_p=x3

        pierwszy=0
        uchwyt.write("</table><br><br>")


print koniec

uchwyt.write(html_pisz)

uchwyt.close()


#szukanie pkt przegiec
"""
licznik = od
pierwszy=0
float(licznik)
float(przeskok)

while True:
    if pierwszy==0:
        pierwszy=1
    else:
        licznik=licznik+przeskok

        funkcja_test=funkcja
        funkcja_test=funkcja_test.replace('x',str(licznik))
        print funkcja_test

    try:
        wynik_funk = eval(funkcja_test)
        print wynik_funk
        float(wynik_funk)
    except:
        wynik_funk = "error"

    licznik = float(round(licznik,10))
    wynik_funk = float(wynik_funk)


    print "%s -> %s" % (licznik,wynik_funk)

    raw_input()


    if round(float(licznik),10)==round(float(do),10):
        break








print "dziala"
raw_input()

"""

raw_input()
