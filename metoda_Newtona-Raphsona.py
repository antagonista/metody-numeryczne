from math import *
import re
import os
import time
from decimal import *


def licz_funkcje(funkcja,x):

    funkcja_n = funkcja.replace('$x',str(x))

    try:
        funkcja_n = eval("Decimal("+funkcja_n+")")
    except:
        funkcja_n = None

    return funkcja_n


separator = "-"
separator=separator.center(20,separator)

konsola = "Poszukamy sobie pierwiastow funkcji!\nMetoda: Newtona-Raphsona\nAutor: Maksym Kawelski"
print konsola
print separator
"""
# podawanie funkcji przez uzytkownika i sprawdzanie jej poprawnosci pod wzgledem "merytorycznym"
blad=0


while True:
    if blad==1:
        #wystapil blad - ponowna proba
        os.system('CLS')
        blad=0
        print "wystapil blad - brak zmiennej w funkcji!\n\n"
    if blad==3:
        #zly zapis funkcji pod wzgledem merytorycznym
        os.system('CLS')
        blad=0
        print "zly zapis funkcji - sproboj jeszcze raz"

    print "Pamietaj aby zmienna w funkcji x zapisac jako $x!"
    funkcja = raw_input("podaj funkcje f(x)=")

    #szukanie i okreslanie zmiennych w podanej funkcji - uboga wersja, szuka tylko x i sprawdza czy funkcja jest poprawnie podana
    zmienne = re.findall("[x]",str(funkcja))
    ile_zmiennych = len(zmienne)

    if ile_zmiennych==0:
        #brak zmiennej podanej - cofniecie
        blad = 1
    else:
        #wszystko git - sprawdzanie poprawnosci napisanej funkcji
        zmienne=zmienne[0]
        funkcja_test = funkcja
        funkcja_test=funkcja_test.replace(zmienne,'1')

        #print funkcja_test

        try:
            wynik=eval(funkcja_test)
            blad=0
            del(funkcja_test)
            del(ile_zmiennych)
            konsola = "f(x)=%s" % (str(funkcja))
            #print wynik
            break
        except:
            blad=3
"""
# pobieranie funkcji

#konsola += ""

while True:
    print "Pamietaj aby zmienna x podawac w funkcji w takiej formie: $x"
    funkcja = raw_input("Podaj funkcje: f(x)=")

    test_funkcji = licz_funkcje(funkcja,1)

    if test_funkcji!=None:
        del(test_funkcji)
        konsola = konsola+"\n\nZalozenia:\nf(x)=%s" % (funkcja)
        break;
    else:
        print "Nie poprawnie podana funkcja! Sproboj ponownie\n"

os.system('CLS')
print konsola
print separator

# pobieranie pochodnej funkcji


while True:
    print "Pamietaj aby zmienna x podawac w funkcji w takiej formie: $x"
    funkcja_pochodna = raw_input("Podaj pochodna funkcji - f`(x)=")

    test_funkcji = licz_funkcje(funkcja_pochodna,1)

    if test_funkcji!=None:
        del(test_funkcji)
        konsola = konsola+"\nf`(x)=%s" % (funkcja_pochodna)
        break;
    else:
        print "Nie poprawnie podana funkcja! Sproboj ponownie\n"

os.system('CLS')
print konsola
print separator


# pobieranie x'a od ktorego zaczynami iteracje

while True:
    try:
        x_poczatek = float(input("Podaj x od ktorego zaczac iteracje: "))
        break
    except:
        os.system('CLS')
        print konsola
        print separator
        print "podana wartosc musi byc liczba!"

konsola = konsola+"\nPoczatek iteracji od (x): %s" % (x_poczatek)

os.system('CLS')
print konsola
print separator


#utworzenie pliku z wynikami
file_name = time.localtime()
file_name = "%s-%s-%s %s-%s-%s.html" % (file_name[0],file_name[1],file_name[2],file_name[3],file_name[4],file_name[5])
uchwyt = open(file_name,"w+")
info = """
funkcja: f(x)=%s<br>
pochodna: f`(x)=%s<br>
poczatek iteracji: %s<br>
""" % (funkcja,funkcja_pochodna,x_poczatek)
uchwyt.writelines(info)

# szukanie pierwiastow

uchwyt.write("<br><h1>Szukanie pierwiastka - wyniki:</h1><br><table border='1'><tr><td>0</td><td>"+str(x_poczatek)+"</td></tr>")

funkcja_wyjsciowa = "$x - (("+str(funkcja)+")/("+str(funkcja_pochodna)+"))"

x_pop = licz_funkcje(funkcja_wyjsciowa,x_poczatek)

licznik = 0

while True:

    licznik = licznik + 1
    wynik = licz_funkcje(funkcja_wyjsciowa,x_pop)
    #print str(wynik)+" "+str(x_pop)
    uchwyt.write("<table><tr><td>"+str(licznik)+"</td><td>"+str(wynik)+"</td></tr>")



    #konczenie pracy
    if (wynik==x_pop):
        uchwyt.write("</table><br><br><h1>Pierwiastek znajduje sie w: "+str(wynik)+"</h1>")
        konsola += "\n\nPierwiastek w: %s" % (wynik)
        break
    else:
        x_pop=wynik




os.system('CLS')
print konsola
#print separator

#print koniec

#uchwyt.write(html_pisz)

uchwyt.close()


#szukanie pkt przegiec
"""
licznik = od
pierwszy=0
float(licznik)
float(przeskok)

while True:
    if pierwszy==0:
        pierwszy=1
    else:
        licznik=licznik+przeskok

        funkcja_test=funkcja
        funkcja_test=funkcja_test.replace('x',str(licznik))
        print funkcja_test

    try:
        wynik_funk = eval(funkcja_test)
        print wynik_funk
        float(wynik_funk)
    except:
        wynik_funk = "error"

    licznik = float(round(licznik,10))
    wynik_funk = float(wynik_funk)


    print "%s -> %s" % (licznik,wynik_funk)

    raw_input()


    if round(float(licznik),10)==round(float(do),10):
        break








print "dziala"
raw_input()

"""

raw_input()
